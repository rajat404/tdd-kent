from money import Money, Bank


def test_multiplication():
    five = Money.dollar(5)
    assert Money.dollar(10) == five.times(2)
    assert Money.dollar(15) == five.times(3)


def test_equality():
    assert Money.dollar(5).equals(Money.dollar(5)) is True
    assert Money.dollar(5).equals(Money.dollar(6)) is False
    assert Money.franc(5).equals(Money.dollar(5)) is False


def test_curreny():
    assert 'USD' == Money.dollar(1).get_currency()
    assert 'CHF' == Money.franc(1).get_currency()


def test_simple_addition():
    five = Money.dollar(5)
    sum = five.plus(five)
    bank = Bank()
    reduced = bank.reduce(sum, 'USD')
    assert Money.dollar(10) == reduced
