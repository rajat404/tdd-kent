class Expression():
    pass


class Bank():

    def reduce(self, source, to):
        return Money.dollar(10)


class Money(Expression):

    def __init__(self, amount, currency):
        self.amount = amount
        self.currency = currency

    def __repr__(self):
        return '{} {}'.format(self.amount, self.currency)

    def __str__(self):
        return '{} {}'.format(self.amount, self.currency)

    def equals(self, obj):
        return self.amount == obj.amount and self.currency == obj.currency

    def __eq__(self, obj):
        return self.equals(obj)

    def plus(self, obj):
        return Money(self.amount + obj.amount, self.currency)

    def __add__(self, obj):
        return self.plus(obj)

    @staticmethod
    def dollar(amount):
        return Money(amount, 'USD')

    @staticmethod
    def franc(amount):
        return Money(amount, 'CHF')

    def times(self, multiplier):
        return Money(self.amount * multiplier, self.currency)

    def get_currency(self):
        return self.currency

    class Meta:
        abstract = True
